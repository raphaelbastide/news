# Déclin Séquence

## 01 November 2020

Déclin Séquence is a version based composition made with CSS animations and transitions, developed each day of October 2020.

> https://declin-sequence.neocities.org/
