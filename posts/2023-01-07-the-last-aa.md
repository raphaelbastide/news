# The last AA (track release)

## 13 february 2023

My first track on Bandcamp is out! It’s called _The last AA_ and it’s a 48 minutes live improvisation on two vintage keyboards, recorded at Modulation 2022, Mijanès, FR.

> https://raphalbastide.bandcamp.com/track/the-last-aa