# Montreuil: 90 seconds to midnight

## 6 june 2024

I will perform an ambient set for the vernissage of *90 seconds to midnight* at 7:30pm in the Tour Orion, Montreuil. Curation Ana Bujošević. RSVP here: https://nonetoile.fr/inscription-event/.

> https://www.instagram.com/p/C7gdknnt1ec/

::live,livecoding