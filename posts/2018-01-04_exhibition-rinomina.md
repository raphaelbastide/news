# Exhibition at Rinomina

## 3 may 2018

I exhibit a piece in the wonderful Rinomina space in Paris, together with Louise Drulhe. Contact me to book a visit from the 3rd to the 17th may.

> http://rinomina.com/exhibition/renaming-the-web/
