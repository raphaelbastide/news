# [cancelled] Talk @ Maison de la Poésie de Rennes

## 30 january 2025

I am giving a talk about my creative process at La Maison de la Poésie de Rennes. This will be part of a residency I am doing there.

> https://maiporennes.fr/residence/2024/11/19/carr-ment-3
