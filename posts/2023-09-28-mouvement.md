# Live coding for Mouvement Magazine

## 4 november 2023

The french magazine _Mouvement_ invited me to play live. From 4PM at Mains d’Œuvres, 1 rue Charles Garnier 93400 Saint-Ouen.

> https://billetterie.mainsdoeuvres.org/evenement/04-11-2023-16-00-mouvement-magazine-s%C3%A9minaire-d-automne

::live,livecoding