# Fungal

## 28 september 2022

After an invitation by Hato Press to make a zine, I decided to pay tribute to the most amazing website ever: Wikipedia. Visit [fungal.page](https://fungal.page/), the type specimen, or buy the fanzine [here](https://hato.store/collections/hato-press-zine-series/products/no-31-fungal-by-raphael-bastide)

> https://hato.store/collections/hato-press-zine-series/products/no-31-fungal-by-raphael-bastide