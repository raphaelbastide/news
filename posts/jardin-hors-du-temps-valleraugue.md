# Valleraugue, Cévennes

## 10 september 2024

Another ambient set, this time in the middle of the Cévennes. See you at 7PM for an introduction talk, in the Jardin Hors du Temps, in Valleraugue, Cévennes (FR).

> https://www.instagram.com/horsdutemps.a.valdaigoual/

::live,livecoding