# Conditions Of Groove

## 3 november 2017

Solo show at 22 Rue Muller in Paris. Opening on the 3rd of November 6pm. November 7th: 7pm Talk of Félix Lambert “Writing in loop: unpredictability and aesthetics of cyclical narrative systems”. November 8th: 7pm Talk of Julien Gargot “The time of Git: Introducing a version management system and its temporalities.” 8:30pm performance of Raphaël Bastide “Conditions of Groove”. Check the link for details.

> http://22ruemuller.com/
