# Festival non-étoile

## 6 july 2024

Algofolk at 4PM in the Tour Orion, Montreuil, for Festival Non-étoile. RSVP here: https://nonetoile.fr/inscription-event/.

> https://nonetoile.fr/festival/

::live,livecoding