# Raphaël Bastide News

Flat file news page with RSS. Works with PHP.

## Use it

For each news, add a markdown (.md) file following this template.

```
# Zone live at Armony, Montreuil

## 20 december 2023

I am playing with my band ZONE on the 20th of december at Armony in Montreuil, FR. We will share the night with Traumpunkt.

> https://raphaelbastide.com/zone/img/poster-armony-web.jpg
```

- The date line will be a [string to time](https://www.php.net/manual/en/function.strtotime.php), so you can write everything that is compatible with this php function.
- The last line can be a URL that will be autolinked, and if in a blockquote ("> " in markdown), will be used for the post link.
- Depending on the date of the event, a class will be to the event if it is in the past or in the future. That will allow styling events accordingly.
- Rename `app/_config.ini` to `app/config.ini` and edit it.

## Inspiration

https://tutorialzine.com/2013/03/simple-php-blogging-system

## License

[GNU AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html)