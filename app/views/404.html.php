<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>404 Not Found | <?php echo config('blog.title') ?></title>
</head>
<body>

	<div class="center message">
		<h1>This page doesn't exist!</h1>
	</div>

</body>
</html>
