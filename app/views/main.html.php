<?php
$now = time();

function compareEventDates($a, $b) {
    $a_time = strtotime($a->eventDate);
    $b_time = strtotime($b->eventDate);
    return $b_time - $a_time;
}

usort($posts, 'compareEventDates');

foreach($posts as $p):
    $rawEventTime = strtotime($p->eventDate);
    if ($now >= $rawEventTime){
      $age = "past";
    } else {
      $age = "future";
    }
    $categories = $p->categories;
    $category_list = implode(' ', $categories);
?>
    <section class="post <?php echo $age." ".$category_list ?>" id="id-<?php echo $p->date ?>">
        <header>
            <h1><?php echo $p->title ?></h1>
            <div class="date">Posted on: <?php echo date('d F Y', $p->date)?></div>
            <div class="event-date"><?php echo $p->eventDate ?></div>
        </header>
        <?php echo $p->body ?>
    </section>
<?php endforeach;?>

<?php if ($has_pagination['prev']):?>
	<a href="?page=<?php echo $page-1?>" class="pagination-arrow newer">Newer</a>
<?php endif;?>

<?php if ($has_pagination['next']):?>
	<a href="?page=<?php echo $page+1?>" class="pagination-arrow older">Older</a>
<?php endif;?>
