<!DOCTYPE html>
<html>
<head>
	<title><?php echo isset($title) ? _h($title) : config('blog.title') ?></title>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" user-scalable="no" />
	<link rel="shortcut icon" href="https://raphaelbastide.com/img/favicon.png"/>
	<meta property="og:url" content="https://raphaelbastide.com/" />
	<meta property="og:image" content="https://raphaelbastide.com/img/og.png" />
	<meta name="twitter:image" content="https://raphaelbastide.com/img/og.png" />
	<meta name="description" content="<?php echo config('blog.description')?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php echo config('blog.title')?>  Feed" href="<?php echo site_url()?>rss" />
	<link rel="stylesheet" href="<?php echo site_url()?>assets/font/stylesheet.css">
	<link rel="stylesheet" href="https://raphaelbastide.com/css/reset.css">
	<link rel="stylesheet" href="https://raphaelbastide.com/css/style.css">
	<style>

	.past{opacity:.4;}
	section{margin: 0 0 40px 0; width:min(800px, 100%);}
	main{margin-top:calc(var(--headerH) * 2);flex-direction: column; align-items: flex-start}
	section header{margin: var(--p) 0 calc(var(--p)/3) 0;}
	.post .date{display: none;}
	.post header{display: flex; flex-wrap:wrap; justify-content: space-between;}
	input{border:1px solid gray; font-size:inherit;}
	@media only screen and (max-width: 600px) {		
		.post .event-date{width:100%; margin-bottom:5px;}
		header nav{flex-wrap: wrap;}
		header h1{width:100%;}
	}

	</style>
</head>
<body>
	<header class="mainheader">
		<nav id="header-list" class="header">
			<h1>Upcoming and <span class="past">past</span> news</h1>
			<a href="https://raphaelbastide.com">Back</a>
			<a class="rss" href="https://news.raphaelbastide.com/rss">RSS</a>
			<a class="rss" href="https://raphaelbastide.com/newsletter">Newsletter</a>
		</nav>
	</header>
	<main>
		<?php echo content()?>
	</main>
	<script src="assets/js/autolinker.js" charset="utf-8"></script>
	<script type="text/javascript">
	var text = document.querySelectorAll('.post')
	for (var i = 0; i < text.length; i++) {
		text[i].innerHTML = Autolinker.link( text[i].innerHTML );
	}
	</script>
</body>
</html>
